import { observable } from 'mobx';

export default class QuatationModel {
    @observable name;

    @observable last;

    @observable highestBid;

    @observable percentChange;

    constructor(obj) {
        this.name = obj.name;
        this.last = obj.last;
        this.highestBid = obj.highestBid;
        this.percentChange = obj.percentChange;
    }

    toJs() {
        return {
            name: this.name,
            last: this.last,
            highestBid: this.highestBid,
            percentChange: this.percentChange
        };
    }
}

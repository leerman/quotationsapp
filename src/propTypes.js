import PropTypes from 'prop-types';

const listItem = {
    name: PropTypes.string.isRequired,
    last: PropTypes.string.isRequired,
    highestBid: PropTypes.string.isRequired,
    percentChange: PropTypes.string.isRequired
};
export default {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired
    }),
    listItem,
    QuatationsStore: PropTypes.shape({
        fetchItemsFromServer: PropTypes.func.isRequired,
        items: PropTypes.arrayOf(PropTypes.shape(listItem)).isRequired,
        isLoading: PropTypes.bool.isRequired,
        error: PropTypes.string
    })
};

import { createStackNavigator } from 'react-navigation';
import About from './About';
import Quatations from './Quatations';

export default createStackNavigator({
    About: {
        screen: About,
        navigationOptions: {
            title: 'О приложении'
        }
    },
    Quatations: {
        screen: Quatations,
        navigationOptions: {
            title: 'Котировки'
        }
    }
});

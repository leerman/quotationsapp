import React, { Component } from 'react';
import { Provider } from 'mobx-react';
import Routes from './Routes';
import QuatationsStore from './stores/QuatationsStore';

const quatationsStore = new QuatationsStore();

export default class App extends Component {
    render() {
        return (
            <Provider QuatationsStore={quatationsStore}>
                <Routes />
            </Provider>
        );
    }
}

import { observable, action, computed } from 'mobx';
import fetchItems from '../network';
import QuatationModel from '../models/QuatationModel';

export default class QuatationsStore {
    @observable items = [];

    @observable error = null;

    @observable loading = true;

    @computed get isLoading() {
        return this.loading && this.items.length === 0;
    }

    @action.bound
    async fetchItemsFromServer() {
        const { error, result } = await fetchItems();
        if (error) {
            this.error = error;
        } else {
            this.error = null;
            this.items = result.map(item => new QuatationModel(item));
        }
    }
}

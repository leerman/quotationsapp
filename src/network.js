export default async () => {
    const response = await fetch('https://poloniex.com/public?command=returnTicker', {
        method: 'GET'
    });
    if (response.status === 200) {
        const result = await response.json();
        const arr = Object.keys(result).map(name => ({
            name,
            ...result[name]
        }));
        return { result: arr };
    }

    return { error: response.statusText };
};

import { StyleSheet } from 'react-native';
import vars from '../constants';

export default StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        justifyContent: 'space-between',
        backgroundColor: vars.backgroundColor
    },
    appName: {
        textAlign: 'center',
        fontSize: 24,
        paddingTop: 10
    },
    appDescription: {
        textAlign: 'center',
        fontSize: 18,
        paddingTop: 20,
        color: vars.subtextColor
    },
    button: {
        paddingBottom: 30
    }
});

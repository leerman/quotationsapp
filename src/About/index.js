import React, { PureComponent } from 'react';
import { View, Text, Button } from 'react-native';
import { aboutText } from '../constants';
import styles from './styles';
import PropTypes from '../propTypes';

export default class About extends PureComponent {
    static propTypes={
        navigation: PropTypes.navigation.isRequired
    }

    onGotoQuatations=() => {
        this.props.navigation.navigate('Quatations');
    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Text style={styles.appName}>
                        Наименование приложения
                    </Text>
                    <Text style={styles.appDescription}>
                        {aboutText}
                    </Text>
                </View>
                <View style={styles.button}>
                    <Button title="Start using" onPress={this.onGotoQuatations} />
                </View>
            </View>
        );
    }
}

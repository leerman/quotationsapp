import React, { PureComponent } from 'react';
import { View, Text, Animated } from 'react-native';
import styles from './styles';
import PropTypes from '../propTypes';

export default class ListItem extends PureComponent {
    static propTypes = {
        ...PropTypes.listItem
    }

    constructor(props) {
        super(props);

        const percent = this.correctPrecent(props.percentChange);
        this.percent = new Animated.Value(percent);
        this.percent.addListener(this.percentChangeListener);

        this.state = {
            percent
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.percentChange !== this.props.percentChange) {
            Animated.timing(this.percent, {
                duration: 1500,
                toValue: this.correctPrecent(nextProps.percentChange)
            }).start();
        }
    }

    componentWillUnmount() {
        this.percent.stopAnimation();
        this.percent.removeAllListeners();
        this.percent = null;
    }

    getPercentString(value) {
        return value.toFixed(8).toString();
    }

    getPercentFloat(percent) {
        return typeof percent === 'string'
            ? parseFloat(percent)
            : percent;
    }

    percentChangeListener=({ value }) => {
        this.setState(() => ({
            percent: this.getPercentString(value)
        }));
    }

    correctPrecent(percentChange) {
        const percent = this.getPercentFloat(percentChange);

        return percent < 0
            ? -1 * percent
            : percent;
    }

    render() {
        const {
            name, last, highestBid, percentChange
        } = this.props;

        const color = this.getPercentFloat(percentChange) < 0
            ? styles.listItemPrecentDown
            : styles.listItemPrecentUp;

        return (
            <View style={styles.listItemContainer}>
                <View style={styles.listItemLeft}>
                    <Text style={styles.listItemName}>
                        { name }
                    </Text>
                    <Text style={[styles.listItemPrecent, color]}>
                        { this.state.percent }%
                    </Text>
                </View>
                <View style={styles.listItemRight}>
                    <Text style={styles.listItemInfoText}>
                  Последняя ставка
                    </Text>
                    <Text style={styles.listItemInfoValue}>
                        { last }
                    </Text>
                    <Text style={styles.listItemInfoText}>
                  Наивысшая ставка
                    </Text>
                    <Text style={styles.listItemInfoValue}>
                        { highestBid }
                    </Text>
                </View>
            </View>
        );
    }
}

import { StyleSheet } from 'react-native';
import vars from '../constants';

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        backgroundColor: vars.backgroundColor
    },
    listItemContainer: {
        backgroundColor: 'white',
        marginVertical: 5,
        padding: 10,
        borderRadius: 3,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    listItemLeft: {
        flex: 1,
        justifyContent: 'space-between'
    },
    listItemRight: {
        alignItems: 'flex-end'
    },
    listItemName: {
        fontWeight: 'bold',
        fontSize: 26
    },
    listItemInfoText: {
        fontSize: 14,
        color: '#A4A4A4'
    },
    listItemInfoValue: {
        fontSize: 18,
        color: vars.subtextColor
    },
    listItemPrecent: {
        fontSize: 22
    },
    listItemPrecentUp: {
        color: '#058941'
    },
    listItemPrecentDown: {
        color: '#BE1527'
    },
    errorContainer: {
        position: 'absolute',
        top: 5,
        alignSelf: 'center',
        backgroundColor: '#FA8072',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5,
        borderColor: '#FB998E',
        borderWidth: 1,
        maxWidth: 250
    },
    errorText: {
        color: 'white',
        fontSize: 16
    },
    activityIndicator: {
        justifyContent: 'center',
        flex: 1
    }
});

import React, { Component } from 'react';
import {
    View, FlatList, Text, ActivityIndicator
} from 'react-native';
import { observer } from 'mobx-react';
import PropTypes from '../propTypes';
import ListItem from './ListItem';
import styles from './styles';

@observer(['QuatationsStore'])
export default class Quatations extends Component {
    static propTypes = {
        navigation: PropTypes.navigation.isRequired,
        QuatationsStore: PropTypes.QuatationsStore.isRequired
    }

    componentDidMount() {
        this.updateQuitations();
        this.timer = setInterval(() => this.updateQuitations(), 5000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
        this.timer = null;
    }

    onGotoAbout = () => {
        this.props.navigation.navigate('About');
    }

    updateQuitations = async () => {
        const { fetchItemsFromServer } = this.props.QuatationsStore;
        fetchItemsFromServer();
    }

    keyExtractor = item => item.name

    renderItem = ({ item }) => (
        <ListItem {...item} />
    )

    render() {
        const { items, error, isLoading } = this.props.QuatationsStore;

        return (
            <View style={styles.container}>
                {isLoading
                    ? (
                        <View style={styles.activityIndicator}>
                            <ActivityIndicator />
                        </View>)
                    : (
                        <FlatList
                            data={items}
                            renderItem={this.renderItem}
                            keyExtractor={this.keyExtractor} />
                    )}
                {error
                && (
                    <View style={styles.errorContainer}>
                        <Text style={styles.errorText}>
                            { error }
                        </Text>
                    </View>
                )}
            </View>
        );
    }
}
